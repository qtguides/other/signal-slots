#ifndef COUNTER_H
#define COUNTER_H

#include <QObject>

class Counter : public QObject
{
    Q_OBJECT
public:
    explicit Counter(QString name, QObject *parent = nullptr);

signals:
    void valueChanged(int newValue); // Cuando el valor cambie vamos a emitir esta señal.

public slots:
    void setValue(int value);

private:
    int value;
    QString name;
};

#endif // COUNTER_H
