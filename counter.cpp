#include "counter.h"
#include <QDebug>

Counter::Counter(QString name, QObject *parent) : QObject(parent)
{
    this->value = 0;
    this->name = name;
}

void Counter::setValue(int value)
{
    if (this->value != value) {
        this->value = value;
        qDebug() << this->name << ": " << this->value;
        emit valueChanged(value);
    }
}
