#include <QCoreApplication>
#include "counter.h"
#include <QDebug>

int main()
{
    Counter a("a"), b("b");

    // Conecta al objeto a, para que al cambiar de valor notifique al objeto b.
    QObject::connect(&a, &Counter::valueChanged, &b, &Counter::setValue);

    a.setValue(12); // a.value() == 12 ---> b.value() == 12
    b.setValue(48); // b.value() == 48 ---> nada

    a.setValue(4);  // a.value() == 4 ---> b.value() == 4
    return 0;
}


/*
int main()
{
    MiApp app;

    return 0;
}
*/

/*
// https://riptutorial.com/es/qt/example/17048/conexion-de-senales---slots-sobrecargados

#include <QObject>

class MyObject : public QObject
{
    Q_OBJECT
public:
    explicit MyObject(QObject *parent = nullptr) : QObject(parent) {}

public slots:
    void slot(const QString &string) { qDebug() << string; }
    void slot(const int integer) {  qDebug() << integer; }

signals:
    void signal(const QString &string);
    void signal(const int integer);
};

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    // using pointers to make connect calls just a little simpler
    MyObject *a = new MyObject;
    MyObject *b = new MyObject;

    // COMPILE ERROR! the compiler does not know which overloads to pick :(
    //QObject::connect(a, &MyObject::signal, b, &MyObject::slot);

    // this works, now the compiler knows which overload to pick, it is very ugly and hard to remember though...
    QObject::connect(
        a, static_cast<void(MyObject::*)(int)>(&MyObject::signal),
        b, static_cast<void(MyObject::*)(int)>(&MyObject::slot));

    // ...so starting in Qt 5.7 we can use qOverload and friends:
    // this requires C++14 enabled:
    QObject::connect(
        a, qOverload<int>(&MyObject::signal),
        b, qOverload<int>(&MyObject::slot));

    // this is slightly longer, but works in C++11:
    QObject::connect(
        a, QOverload<int>::of(&MyObject::signal),
        b, QOverload<int>::of(&MyObject::slot));

    // there are also qConstOverload/qNonConstOverload and QConstOverload/QNonConstOverload,
    // the names should be self-explanatory
}
*/

